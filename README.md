# arrosage ecolomatique

programmation nano arduino afin de : 
- étape 1 (2020)  = réalisé
si basse luminosité, vérification taux d'humidité terre plante témoin; si inférieur seuil bas, arrosage automatique jusqu'à atteinte seuil haut  
- étape 2 (2021) = non réalisé
remplissage automatique du réservoir de récupération d'eau de pluie par eau de ville quand niveau insuffisant  

## matériel étape 1

- Arduino compatible Nano with CH340 USB IC
- lumino résistance jour/nuit
- iHaospace Capacitive Soil Moisture Sensor V1.2
- électrovanne claber 90814 24 V AC (0,5 - 12 bar, 4,6 - 96 l/m)
- relais SRD 05VDC-SL-C (commande électrovanne)
- transfo 220/5 V (alimentation arduino)
- transfo 200/24 V (alimentation électrovanne)
- toit 1,5 m² + gouttière + conduite et connecteurs
- récupérateur eau de pluie 300 l
- collecteur diamètre 50 cm (raccord conduite/récupérateur)


## code étape 1

- acquistion capteur luminosité = condition pour entrer dans la boucle humidité : ok
- acquisition capteur humidité et déclenchement clignotement led : ok  
- transformation code clignotement led => ouverture/fermeture électrovanne : ok

